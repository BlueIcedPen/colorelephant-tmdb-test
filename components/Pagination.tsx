import React from 'react'

interface Props {
  postsPerPage: number
  currentPage: number
  totalPosts: number
  paginate: Function
}

const Pagination: React.FC<Props> = ({ 
  postsPerPage, 
  totalPosts, 
  currentPage, 
  paginate
}) => {
  const pageNumbers = []

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i)
  }
  return (
    <nav className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px' aria-label='Pagination'>
      {pageNumbers.length > 1 && (
        <a
          href='#'
          onClick={paginate(1)}
          className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
          <span className='sr-only'>Previous</span>
          <svg
            className='h-5 w-5'
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 20 20'
            fill='currentColor'
            aria-hidden='true'>
            <path
              fillRule='evenodd'
              d='M15.707 15.707a1 1 0 01-1.414 0l-5-5a1 1 0 010-1.414l5-5a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 010 1.414zm-6 0a1 1 0 01-1.414 0l-5-5a1 1 0 010-1.414l5-5a1 1 0 011.414 1.414L5.414 10l4.293 4.293a1 1 0 010 1.414z'
              clipRule='evenodd'
            />
          </svg>
        </a>
      )}
      {pageNumbers.map((number) => {
        if (number === totalPosts || (number >= currentPage - 2 && number <= currentPage + 2)) {
          return (
            <a
              key={number}
              href='#'
              onClick={paginate(number)}
              className={`${
                number === currentPage
                  ? 'z-10 bg-indigo-50 border-indigo-500 text-indigo-600 '
                  : 'bg-white border-gray-300 text-gray-500 hover:bg-gray-50 '
              }relative inline-flex items-center px-4 py-2 border text-sm font-medium`}>
              {number}
            </a>
          )
        }
      })}
      {pageNumbers.length > 1 && (
        <a
          href='#'
          onClick={paginate(25)}
          className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
          <span className='sr-only'>Next</span>
          <svg
            className='h-5 w-5'
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 20 20'
            fill='currentColor'
            aria-hidden='true'>
            <path
              fillRule='evenodd'
              d='M10.293 15.707a1 1 0 010-1.414L14.586 10l-4.293-4.293a1 1 0 111.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z'
              clipRule='evenodd'
            />
            <path
              fillRule='evenodd'
              d='M4.293 15.707a1 1 0 010-1.414L8.586 10 4.293 5.707a1 1 0 011.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z'
              clipRule='evenodd'
            />
          </svg>
        </a>
      )}
    </nav>
  )
}

export default Pagination

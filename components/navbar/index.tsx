import React from 'react'

const Navbar: React.FC = () => {
  return (
    <>
      <nav className='bg-gray-800'>
        <div className='max-w-7xl mx-auto px-2 sm:px-6 lg:px-8'>
          <div className='relative flex items-center justify-between h-16'>
            <div className='flex-1 flex items-center justify-center sm:items-stretch sm:justify-start'>
              <div className='flex-shrink-0 flex items-center'>
                <img
                  className='block lg:hidden h-8 w-auto'
                  src='https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg'
                  alt='tmdb-test'
                />
                <img
                  className='hidden lg:block h-8 w-auto'
                  src='https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg'
                  alt='tmdb-test'
                />
              </div>
            </div>
            <div className='absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0'>
              <div className='ml-3 relative'>
                <div>
                  <button
                    type='button'
                    className='bg-gray-800 flex text-sm rounded-full focus:outline-none'
                    aria-expanded='false'>
                    <img
                      className='h-8 w-8 rounded-full'
                      src='https://www.gladwellacademy.com/wp-content/themes/smartlearning-child/images/trainer-profile-placeholder.svg'
                      alt=''
                    />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar;

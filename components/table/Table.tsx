import React, { useEffect, useState } from 'react'
import { get } from 'lodash'
import { MovieListInfo } from '../../pages/api/services'
import Pagination from '../Pagination'
import { COOKIE_NAME, ESort, MoviesResult, tableRowPlaceHolder } from '../../config'
import { TableHead, TableRow } from '.'

interface Props {
  moviesResult: MovieListInfo | undefined
  api: Function
  loading: boolean
}

const Columns = ['Movie', 'Release Year', 'Rating (100%)']

const Table: React.FC<Props> = ({ moviesResult, api, loading = true }) => {
  const [favorites, setFavorites] = useState<number[]>([])

  const getPercentage = (value: number): number => value * 10

  const sortBy = async (value: ESort): Promise<void> => {
    await api({ sort: value })
  }

  const changePage = (page: number) => async (e: React.MouseEvent) => {
      e.preventDefault()
      console.log(page)
      await api({ page })
    }

  const handleFavourite = (value: number): void => {
    let cookies: number[] = getFromStorage()
    if (cookies && cookies.length) {
      if (cookies.includes(value)) {
        cookies = removeItem(cookies, value)
        localStorage[COOKIE_NAME] = JSON.stringify(cookies)
        setFavorites(cookies)
      } else {
        cookies.push(value)
        addItem(cookies)
        setFavorites(cookies)
      }
    } else {
      setFavorites([value])
      addItem([value])
    }
  }

  function getFromStorage(): number[] {
    const selectedItems = localStorage[COOKIE_NAME]
    if (selectedItems) return JSON.parse(selectedItems)
    return []
  }

  function removeItem(arr: number[], item: number) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] === item) {
        arr.splice(i, 1)
        favorites.splice(i, 1)
        i--
      }
    }
    return arr
  }

  function addItem(arr: number[]) {
    localStorage[COOKIE_NAME] = JSON.stringify(arr)
  }

  useEffect(() => {
    setFavorites(getFromStorage())
    return () => {}
  }, [])

  return (
    <div className='container mx-auto px-4 sm:px-8'>
      <div className='py-8'>
        <div>
          <h2 className='text-2xl font-semibold leading-tight'> Top 500 Movies</h2>
        </div>
        <div className='my-3 flex sm:flex-row flex-col'>
          <div className='flex flex-row mb-1 sm:mb-0'>
            <div className='relative'>
              <select
                className='appearance-none h-full rounded-l rounded-r border block appearance-none w-full bg-white border-gray-400 text-gray-700 py-2 px-4 pr-8 focus:outline-none focus:bg-white focus:border-gray-500'
                placeholder='Sort by'
                defaultValue={undefined}
                onChange={(e) => {
                  switch (e.target.value) {
                    case 'asc':
                      sortBy(ESort.ASC)
                      break
                    case 'desc':
                      sortBy(ESort.DESC)
                      break
                    default:
                      break
                  }
                }}>
                <option value={undefined}>Sort By</option>
                <option value={ESort.ASC}>Ascending</option>
                <option value={ESort.DESC}>Descending</option>
              </select>
              <div className='pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700'>
                <svg className='fill-current h-4 w-4' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                  <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                </svg>
              </div>
            </div>
          </div>
        </div>
        <div className='-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto'>
          <div className='inline-block min-w-full shadow rounded-lg overflow-hidden'>
            <table className='min-w-full leading-normal'>
              <TableHead columns={Columns} />
              <tbody>
                {loading
                  ? tableRowPlaceHolder([...new Array(20).fill({})])
                  : get(moviesResult, 'movies', []).map((movie: MoviesResult, key: number) => {
                      return (
                        <TableRow key={key} movie={movie} favorites={favorites} handleFavourite={handleFavourite} />
                      )
                    })}
              </tbody>
            </table>

            <div className='bg-white px-4 py-3 flex items-center justify-between sm:px-6'>
              <div className='flex-1 flex justify-between sm:hidden'>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'>
                  Previous
                </a>
                <a
                  href='#'
                  className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'>
                  Next
                </a>
              </div>
              <div className='px-6 py-2 hidden sm:flex-1 sm:flex sm:items-center sm:justify-between'>
                <div>
                  <p className='text-sm text-gray-700'>
                    <span className='font-medium'>{20 * (get(moviesResult, 'currPage') || 1) - 19} </span>
                    to
                    <span className='font-medium'> {20 * (get(moviesResult, 'currPage') || 1)} </span>
                    of
                    <span className='font-medium'> 500</span>
                  </p>
                </div>
                <div>
                  <Pagination
                    postsPerPage={20}
                    currentPage={get(moviesResult, 'currPage') || 1}
                    totalPosts={500}
                    paginate={changePage}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Table

import React from 'react'
import { IMAGES_ENDPOINT, MoviesResult, TMDB_MOVIE_ENDPOINT } from '../../config'

interface Props {
  movie: MoviesResult
  favorites: number[]
  handleFavourite: Function
}

const TableRow: React.FC<Props> = ({ movie, favorites, handleFavourite }) => {
  const getPercentage = (value: number): number => value * 10

  return (
    <tr className={`${favorites.includes(movie.id) ? 'bg-aliceblue' : ''}`}>
      <td className='px-5 py-5 border-b border-gray-200 text-sm'>
        <div className='flex items-center'>
          <div className='flex-shrink-0 w-10'>
            <img
              className='w-full h-full rounded-half'
              src={
                movie.poster_path ? `${IMAGES_ENDPOINT}/${movie.poster_path}` : 'https://neurosoft.com/img/notfound.png'
              }
              alt=''
            />
          </div>
          <div className='ml-3'>
            <p className='text-gray-900 whitespace-no-wrap'>{movie.title}</p>
          </div>
          <a className='px-3' href={`${TMDB_MOVIE_ENDPOINT}/${movie.id}`}>
            <svg xmlns='http://www.w3.org/2000/svg' className='h-5 w-5' viewBox='0 0 20 20' fill='blue'>
              <path d='M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z' />
              <path d='M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z' />
            </svg>
          </a>
        </div>
      </td>
      <td className='px-5 py-5 border-b border-gray-200 text-sm'>
        <p className='text-gray-900 whitespace-no-wrap'>{movie.release_date.toString().split('-')?.[0] || ' - '}</p>
      </td>
      <td className='px-5 py-5 border-b border-gray-200 text-sm'>
        <span className='relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight'>
          <span aria-hidden className='absolute inset-0 bg-green-200 opacity-50 rounded-full'></span>
          <span className='relative'>{`${getPercentage(movie.vote_average)}%`}</span>
        </span>
      </td>
      <td className='border-b border-gray-200 text-sm'>
        <button
          className='relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight'
          onClick={() => handleFavourite(movie.id)}>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='h-6 w-6'
            fill={favorites.includes(movie.id) ? 'gold' : 'none'}
            viewBox='0 0 24 24'
            stroke='gold'>
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z'
            />
          </svg>
        </button>
      </td>
    </tr>
  )
}

export default TableRow

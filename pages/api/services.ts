import axios from 'axios'
import { MoviesResult, MovieFetchResults, POPULAR_MOVIES_API, API_KEY, ESort } from '../../config'

export interface MovieListInfo {
  movies?: MoviesResult[]
  currPage?: number
  totalPages?: number
}

export interface MovieFetchQuery {
  page?: number
  sort: ESort
}

export const fetchMovies = (params: MovieFetchQuery): Promise<MovieListInfo> => {
  let movieListInfo: MovieListInfo = {}
  return axios
    .get(
      `${POPULAR_MOVIES_API}?api_key=${API_KEY}&page=${
        params.page ? params.page : 1
      }&sort_by=popularity.${params.sort ? params.sort : ESort.DESC}`
    )
    .then((data) => data.data)
    .then((data: MovieFetchResults) => {
      console.log(data.results)
      movieListInfo = {
        movies: data.results,
        currPage: Math.max(1, data.page),
        totalPages: Math.max(1, data.total_pages)
      }
      movieListInfo = movieListInfo
      return movieListInfo
    })
}

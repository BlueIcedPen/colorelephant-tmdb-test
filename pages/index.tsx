import type { NextPage } from 'next'
import { useEffect, useState } from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { Table } from '../components'
import { fetchMovies, MovieFetchQuery, MovieListInfo } from './api/services'
import { ESort } from '../config'


type TFetchList = (x: MovieFetchQuery) => void

const Home: NextPage = () => {
  const [moviesResult, setMoviesResult] = useState<MovieListInfo>()
  const [isLoading, setIsLoading] = useState<boolean>(true)
  
  const fetchList: TFetchList = async ({ page = 1, sort = ESort.DESC }) => {
    setIsLoading(true)
    try {
      const results: MovieListInfo = await fetchMovies({ page, sort })
      setMoviesResult(results)
      setIsLoading(false)
    } catch (error) {
      //TODO: implement
      throw error 
    }
  }
  
  useEffect(() => {
    fetchList({page: 1, sort: ESort.DESC})
    return () => {}
  }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>React Engineer Test</title>
        <meta name='description' content='NextJs typescript app for ColorElephant React Engineer Test' />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <Table moviesResult={moviesResult} api={fetchList} loading={isLoading}/>
      </main>
    </div>
  )
}

export default Home

import React from "react"
import ContentLoader from 'react-content-loader'

export const tableRowPlaceHolder = (items: any) => {
  return (
    <>
      {items.map((e: any, i: number) => (
        <div key={i} className='border-b border-gray-200 text-sm'>
          <ContentLoader
            speed={2}
            width={1080}
            height={100}
            viewBox='0 0 1080 100'
            backgroundColor='#f3f3f3'
            foregroundColor='#ecebeb'>
            <rect x='71' y='47' rx='3' ry='3' width='120' height='8' />
            <rect x='654' y='47' rx='0' ry='0' width='770' height='8' />
            <rect x='220' y='44' rx='3' ry='3' width='14' height='14' />
            <rect x='20' y='20' rx='0' ry='0' width='40' height='60' />
          </ContentLoader>
        </div>
      ))}
    </>
  )
}

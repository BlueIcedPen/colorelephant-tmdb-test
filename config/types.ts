export interface MovieFetchResults {
  page: number;
  results: MoviesResult[];
  total_pages: number;
  total_results: number;
}

export interface MoviesResult {
  adult: boolean;
  backdrop_path: string;
  id: number;
  original_title: string;
  release_date: Date;
  poster_path: string;
  popularity: number;
  title: string;
  vote_average: number;
  vote_count: number;
  name: string;
}

export enum ESort {
  ASC= 'asc',
  DESC= 'desc'
}

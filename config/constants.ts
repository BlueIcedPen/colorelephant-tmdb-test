// Backend Services
export const POPULAR_MOVIES_API: string = 'https://api.themoviedb.org/3/discover/movie';
export const IMAGES_ENDPOINT: string = 'https://image.tmdb.org/t/p/w500/';
export const TMDB_MOVIE_ENDPOINT: string = 'https://www.themoviedb.org/movie';
export const API_KEY: string = '33beb55527f167175a84c2d169947a91';
export const COOKIE_NAME: string = 'FAVORITES';
